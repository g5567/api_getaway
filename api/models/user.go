package models

// OrderModel ...
type UserModel struct {
	ID             string      `json:"id"`
	Name           string      `json:"name"`
	Email     	   string      `json:"email"`
	Phone          string      `json:"phone"`
	Address        string      `json:"address"`
	Longlat        string      `json:"longlat"`
	Books          []UserModel `json:"books"`
	Status         string      `json:"status"`
	CreatedAt      string      `json:"created_at"`
	UpdatedAt      string      `json:"updated_at"`
}

//CreateOrderModel ...
type CreateUserModel struct {
	Name     	   string      `json:"customer_id"`
	Email   	   string      `json:"customer_name"`
	Phone          string      `json:"phone"`
	Address        string      `json:"address"`
	Longlat        string      `json:"longlat"`
}

// //CreateOrderModelFormData ...
// type CreateUserModelFormData struct {
// 	CustomerID     string `form:"customer_id"`
// 	CustomerName   string `form:"customer_name"`
// 	Phone          string `form:"phone"`
// 	Address        string `form:"address"`
// 	Longlat        string `form:"longlat"`
// 	Note           string `form:"note"`
// 	Items          string `form:"items"`
// 	DeliveryMethod string `form:"delivery_method"`
// 	PaymentMethod  string `form:"payment_method"`
// }

//UpdateOrderModel ...
type UpdateOrderModel struct {
	Name     	   string      `json:"customer_id"`
	Email   	   string      `json:"customer_name"`
	Phone          string      `json:"phone"`
	Address        string      `json:"address"`
	Longlat        string      `json:"longlat"`
	Status         string 	   `json:"status"`
}

// // OrderItem ...
type UsersItem struct {
	BookID   	string  `json:"book_id"`
	BookName 	string  `json:"book_name"`
	Image       string  `json:"image"`
	Price       float32 `json:"price"`
	Quantity    int32   `json:"quantity"`
}

// FindOrdersModel - request for find method
type FindUserModel struct {
	Page   int64  `json:"page,string"`
	Search string `json:"search"`
	Limit  int64  `json:"limit,string"`
}

// OrdersModel - response for find request
type OrdersModel struct {
	Users []UserModel 	`json:"orders"`
	Count  int64        `json:"count,string"`
}
