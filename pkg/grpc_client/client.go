package grpc_client

import (
	"fmt"
	pb "genproto/catalog_service"
	pbc "genproto/content_service"
	pbe "genproto/excel_service"
	pbo "genproto/order_service"
	pbn "genproto/settings_service"
	pbs "genproto/sms_service"
	pbu "genproto/user_service"

	"google.golang.org/grpc"

	"bitbucket.org/alien_soft/gz_api_gateway/config"
)

//GrpcClientI ...
type GrpcClientI interface {
	CategoryService() pb.CategoryServiceClient
	ProductService() pb.ProductServiceClient
	BrandService() pb.BrandServiceClient
	ProductPropertyService() pb.ProductPropertyServiceClient
	ProductPropertyGroupService() pb.ProductPropertyGroupServiceClient
	ShopService() pb.ShopServiceClient
	FeedbackService() pb.FeedbackServiceClient
	CustomerService() pbu.CustomerServiceClient
	AdminService() pbu.AdminServiceClient
	PermissionService() pbu.PermissionServiceClient
	UserActivityService() pbu.UserActivityServiceClient
	SmsService() pbs.SmsServiceClient
	NewsService() pbc.NewsServiceClient
	PromoService() pbc.PromoServiceClient
	ExcelService() pbe.ExcelServiceClient
	BannerService() pbc.BannerServiceClient
	BannerPositionService() pbc.BannerPositionServiceClient
	PageService() pbc.PageServiceClient
	InstallmentService() pbn.InstallmentServiceClient
	FeaturedListService() pb.FeaturedListServiceClient
}

//GrpcClient ...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

//New ...
func New(cfg config.Config) (*GrpcClient, error) {

	connCatalog, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.CatalogServiceHost, cfg.CatalogServicePort),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("catalog service dial host: %s port:%d err: %s",
			cfg.CatalogServiceHost, cfg.CatalogServicePort, err)
	}

	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.UserServiceHost, cfg.UserServicePort),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %s port:%d err: %s",
			cfg.UserServiceHost, cfg.UserServicePort, err)
	}

	connSms, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.SmsServiceHost, cfg.SmsServicePort),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("sms service dial host: %s port:%d err: %s",
			cfg.SmsServiceHost, cfg.SmsServicePort, err)
	}

	connOrder, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.OrderServiceHost, cfg.OrderServicePort),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("order service dial host: %s port:%d err: %s",
			cfg.OrderServiceHost, cfg.OrderServicePort, err)
	}

	connContent, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.ContentServiceHost, cfg.ContentServicePort),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("Content service dial host: %s port:%d err: %s",
			cfg.ContentServiceHost, cfg.ContentServicePort, err)
	}

	connSettings, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.SettingsServiceHost, cfg.SettingsServicePort),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("Settings service dial host: %s port:%d err: %s",
			cfg.SettingsServiceHost, cfg.SettingsServicePort, err)
	}

	connExcel, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.ExcelServiceHost, cfg.ExcelServicePort),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("Excel service dial host: %s port:%d err: %s",
			cfg.ExcelServiceHost, cfg.ExcelServicePort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"category_service":               pb.NewCategoryServiceClient(connCatalog),
			"product_service":                pb.NewProductServiceClient(connCatalog),
			"brand_service":                  pb.NewBrandServiceClient(connCatalog),
			"product_property_service":       pb.NewProductPropertyServiceClient(connCatalog),
			"product_property_group_service": pb.NewProductPropertyGroupServiceClient(connCatalog),
			"shop_service":                   pb.NewShopServiceClient(connCatalog),
			"feedback_service":               pb.NewFeedbackServiceClient(connCatalog),
			"customer_service":               pbu.NewCustomerServiceClient(connUser),
			"admin_service":                  pbu.NewAdminServiceClient(connUser),
			"permission_service":             pbu.NewPermissionServiceClient(connUser),
			"user_activity_service":          pbu.NewUserActivityServiceClient(connUser),
			"sms_service":                    pbs.NewSmsServiceClient(connSms),
			"order_service":                  pbo.NewOrderServiceClient(connOrder),
			"news_service":                   pbc.NewNewsServiceClient(connContent),
			"promo_service":                  pbc.NewPromoServiceClient(connContent),
			"excel_service":                  pbe.NewExcelServiceClient(connExcel),
			"banner_service":                 pbc.NewBannerServiceClient(connContent),
			"banner_position_service":        pbc.NewBannerPositionServiceClient(connContent),
			"page_service":                   pbc.NewPageServiceClient(connContent),
			"installment_service":            pbn.NewInstallmentServiceClient(connSettings),
			"featured_list_service":          pb.NewFeaturedListServiceClient(connCatalog),
		},
	}, nil
}

// category service
func (g *GrpcClient) CategoryService() pb.CategoryServiceClient {
	return g.connections["category_service"].(pb.CategoryServiceClient)
}

// product service
func (g *GrpcClient) ProductService() pb.ProductServiceClient {
	return g.connections["product_service"].(pb.ProductServiceClient)
}

// brand service
func (g *GrpcClient) BrandService() pb.BrandServiceClient {
	return g.connections["brand_service"].(pb.BrandServiceClient)
}

// product property service
func (g *GrpcClient) ProductPropertyService() pb.ProductPropertyServiceClient {
	return g.connections["product_property_service"].(pb.ProductPropertyServiceClient)
}

// product property group service
func (g *GrpcClient) ProductPropertyGroupService() pb.ProductPropertyGroupServiceClient {
	return g.connections["product_property_group_service"].(pb.ProductPropertyGroupServiceClient)
}

// shop service
func (g *GrpcClient) ShopService() pb.ShopServiceClient {
	return g.connections["shop_service"].(pb.ShopServiceClient)
}

// feedback service
func (g *GrpcClient) FeedbackService() pb.FeedbackServiceClient {
	return g.connections["feedback_service"].(pb.FeedbackServiceClient)
}

// customer service
func (g *GrpcClient) CustomerService() pbu.CustomerServiceClient {
	return g.connections["customer_service"].(pbu.CustomerServiceClient)
}

// AdminService service for managing admins
func (g *GrpcClient) AdminService() pbu.AdminServiceClient {
	return g.connections["admin_service"].(pbu.AdminServiceClient)
}

// UserActivityService service for managing UserActivitis
func (g *GrpcClient) UserActivityService() pbu.UserActivityServiceClient {
	return g.connections["user_activity_service"].(pbu.UserActivityServiceClient)
}

//SmsService ...
func (g *GrpcClient) SmsService() pbs.SmsServiceClient {
	return g.connections["sms_service"].(pbs.SmsServiceClient)
}

// OrderService ...
func (g *GrpcClient) OrderService() pbo.OrderServiceClient {
	return g.connections["order_service"].(pbo.OrderServiceClient)
}

// NewsService ...
func (g *GrpcClient) NewsService() pbc.NewsServiceClient {
	return g.connections["news_service"].(pbc.NewsServiceClient)
}

// PromoService ...
func (g *GrpcClient) PromoService() pbc.PromoServiceClient {
	return g.connections["promo_service"].(pbc.PromoServiceClient)
}

// ExcelService ...
func (g *GrpcClient) ExcelService() pbe.ExcelServiceClient {
	return g.connections["excel_service"].(pbe.ExcelServiceClient)
}

// BannerService ...
func (g *GrpcClient) BannerService() pbc.BannerServiceClient {
	return g.connections["banner_service"].(pbc.BannerServiceClient)
}

// BannerPositionService ...
func (g *GrpcClient) BannerPositionService() pbc.BannerPositionServiceClient {
	return g.connections["banner_position_service"].(pbc.BannerPositionServiceClient)
}

// PageService ...
func (g *GrpcClient) PageService() pbc.PageServiceClient {
	return g.connections["page_service"].(pbc.PageServiceClient)
}

// InstallmentService ...
func (g *GrpcClient) InstallmentService() pbn.InstallmentServiceClient {
	return g.connections["installment_service"].(pbn.InstallmentServiceClient)
}

// FeaturedListService ...
func (g *GrpcClient) FeaturedListService() pb.FeaturedListServiceClient {
	return g.connections["featured_list_service"].(pb.FeaturedListServiceClient)
}

// PermissionService ...
func (g *GrpcClient) PermissionService() pbu.PermissionServiceClient {
	return g.connections["permission_service"].(pbu.PermissionServiceClient)
}
