package main

import (

	//"github.com/casbin/casbin/v2"
	//defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	//"github.com/casbin/casbin/v2/util"
	//gormadapter "github.com/casbin/gorm-adapter/v2"

	// _ "github.com/lib/pq"

	// "bitbucket.org/alien_soft/gz_api_gateway/api"
	"gitlab.com/g5567/api_getaway.git/config"
	// "bitbucket.org/alien_soft/gz_api_gateway/pkg/grpc_client"
	// "bitbucket.org/alien_soft/gz_api_gateway/pkg/logger"
)

var (
	log        logger.Logger
	cfg        config.Config
	grpcClient *grpc_client.GrpcClient
	err        error
	//casbinEnforcer 	*casbin.Enforcer
)

func initDeps() {
	cfg = config.Load()
	log = logger.New(cfg.LogLevel, "gz_api_gateway")

	grpcClient, err = grpc_client.New(cfg)
	if err != nil {
		log.Error("grpc dial error", logger.Error(err))
	}
}

func main() {
	initDeps()

	server := api.New(api.Config{
		Logger:     log,
		GrpcClient: grpcClient,
		Cfg:        cfg,
	})

	server.Run(cfg.HTTPPort)
}
